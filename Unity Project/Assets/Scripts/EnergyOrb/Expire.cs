﻿using UnityEngine;
using System.Collections;

public class Expire : MonoBehaviour 
{
    public float LifeTime = 6f;
    public float BlinkTime = 2f;
    public float BlinkPeriod = 0.1f;

    bool Blinking = false;
    float TimeSinceBlink = 0f;

	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update() 
    {
        LifeTime -= Time.deltaTime;
        if (TimeSinceBlink > 0)
            TimeSinceBlink -= Time.deltaTime;


        if (!Blinking && LifeTime <= BlinkTime)
        {
            Blink();
            Blinking = true;
        }

        if (Blinking)
        {
            if (TimeSinceBlink <= 0)
                Blink();
        }

        if (LifeTime <= 0)
            Destroy(gameObject);
	}

    void FixedUpdate()
    {

    }

    void Blink()
    {
        if (gameObject.renderer.enabled == true)
            gameObject.renderer.enabled = false;
        else
            gameObject.renderer.enabled = true;

        TimeSinceBlink = BlinkPeriod;
    }
}
