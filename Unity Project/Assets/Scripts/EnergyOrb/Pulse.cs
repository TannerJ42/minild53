﻿using UnityEngine;
using System.Collections;

public class Pulse : MonoBehaviour 
{
    public float PulseSpeed;
    public float PulseRange = 0.5f;

    float minimum;
    float maximum;
    float currentSize;
    float direction = 1;

	// Use this for initialization
	void Start () 
    {
        currentSize = transform.localScale.x;
        minimum = currentSize - PulseRange;
        maximum = currentSize + PulseRange;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (direction == 1 &&
            currentSize > maximum)
        {
            direction = -1;
        }
        else if (direction == -1 &&
                currentSize < minimum)
        {
            direction = 1;
        }
	}

    void FixedUpdate()
    {
        currentSize += PulseSpeed * direction;

        transform.localScale = new Vector3(currentSize, currentSize);
    }
}
