﻿using UnityEngine;
using System.Collections;

public class PlayerPickup : MonoBehaviour 
{
    public int Energy = 1;

    void OnTriggerEnter2D(Collider2D c)
    {

        if (c.gameObject.tag == "Player")
        {
            c.gameObject.SendMessage("AddEnergy", Energy);
            Destroy(gameObject);
        }
    }
}
