﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour 
{
    float Timer = 0;

    List<float> StraightShooterTimes;
    List<float> SpinnerTimes;

    public GameObject StraightShooter;
    public GameObject Spinner;

    public GameObject TopBorder;
    public GameObject BottomBorder;

    public List<GameObject> Levels;

	// Use this for initialization
	void Start()
    {
        GameObject GameManager = GameObject.FindGameObjectWithTag("GameManager");
        GameState gameState = GameManager.GetComponent<GameState>();

        int level = gameState.CurrentLevel;

        if (level <= 0)
            level = 1;

        LevelStats stats = Levels[level - 1].GetComponent<LevelStats>();

        StraightShooterTimes = stats.StraightShooters;
        SpinnerTimes = stats.Spinners;
	}
	
	// Update is called once per frame
	void Update()
    {
        Timer += Time.deltaTime;

        CheckForSpawns(StraightShooter, StraightShooterTimes);
        CheckForSpawns(Spinner, SpinnerTimes);
	}

    Vector3 GetStartingPosition()
    {
        return new Vector3(transform.position.x, Random.Range(BottomBorder.transform.position.y, TopBorder.transform.position.y), 0);
    }

    void CheckForSpawns(GameObject prefab, List<float> times)
    {
        if (times.Count > 0)
        {
            while (times[0] <= Timer)
            {
                times.RemoveAt(0);
                SpawnEnemy(prefab);
            }
        }
    }

    void SpawnEnemy(GameObject prefab)
    {
        Instantiate(prefab, GetStartingPosition(), prefab.transform.rotation);
    }
}
