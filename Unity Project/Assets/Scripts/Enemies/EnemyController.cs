﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour 
{
    public GameObject EnergyPrefab;
    public Vector3 Direction;
    public float Speed;
    public int Energy = 1;
    public int HP = 1;

	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update() 
    {
	    
	}

    void FixedUpdate()
    {
        transform.position = transform.position + Direction * Speed * Time.deltaTime;
    }

    public void Fire()
    {

    }

    public void TakeDamage(int damage)
    {
        Debug.Log("Taking damage!");
        HP -= damage;
        if (HP <= 0)
        {
            DropEnergy();
            Destroy(gameObject);
        }
    }

    void DropEnergy()
    {
        GameObject energyDrop = (GameObject)Instantiate(EnergyPrefab, transform.position, transform.rotation);
        PlayerPickup pickup = energyDrop.GetComponent<PlayerPickup>();
        pickup.Energy = Energy;
    }
}
