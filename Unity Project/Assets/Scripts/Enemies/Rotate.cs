﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour 
{

    public float RotationAmount = 0.01f;
    public bool Clockwise = true;

	// Use this for initialization
	void Start()
    {
        if (Random.Range(0, 2) == 1)
            Clockwise = false;
	}
	
	// Update is called once per frame
	void Update() 
    {
	    
	}

    void FixedUpdate()
    {
        float rotation = RotationAmount;

        if (Clockwise)
            rotation = -rotation;

        transform.Rotate(new Vector3(0, 0, rotation));
    }
}
