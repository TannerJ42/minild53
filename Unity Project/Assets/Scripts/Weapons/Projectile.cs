﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
    public bool Player;
    public float Speed;
    public int Damage;

    public Vector3 Direction;
    string Tag = "Player";

	// Use this for initialization
	void Start()
    {
        if (Player)
        {
            Tag = "Enemy";
        }
	}
	
	// Update is called once per frame
	void Update() 
    {   

	}

    public void SetDirection(Vector2 direction)
    {
        Direction = direction;

        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0, 0, angle);
    }

    void FixedUpdate()
    {
        transform.position = transform.position + Direction * Speed * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (Tag == collision.gameObject.tag)
        {
            collision.gameObject.SendMessage("TakeDamage", Damage);
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "Border")
        {
            Destroy(gameObject);
        }
    }
}
