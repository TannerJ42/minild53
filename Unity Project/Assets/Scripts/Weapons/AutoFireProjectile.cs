﻿using UnityEngine;
using System.Collections;

public class AutoFireProjectile : MonoBehaviour
{
    public float FireSpeed;
    public GameObject Projectile;

    float timeSinceFired;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (timeSinceFired >= FireSpeed)
        {
            Fire();
        }
    }

    void FixedUpdate()
    {
        timeSinceFired += Time.deltaTime;
    }

    void Fire()
    {
        timeSinceFired = 0;
        GameObject p = (GameObject)Instantiate(Projectile, new Vector3(transform.position.x, transform.position.y, transform.position.z + 1), transform.rotation);
        Projectile projectile = p.GetComponent<Projectile>();

        float Angle = transform.rotation.eulerAngles.z;
        
        float radians = Angle * Mathf.Deg2Rad;

        projectile.SetDirection(new Vector2((float)Mathf.Cos(radians), (float)Mathf.Sin(radians)));
    }
}
