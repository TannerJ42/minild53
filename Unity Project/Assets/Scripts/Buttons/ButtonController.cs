﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour
{
    public GameObject PressedSprite;
    public GameObject UnPressedSprite;
    public GameObject HighlightSprite;

    // Use this for initialization
    void Start()
    {
        HighlightSprite.renderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Press()
    {
        UnPressedSprite.renderer.enabled = false;
    }

    public void UnPress()
    {
        UnPressedSprite.renderer.enabled = true;
    }

    public void Highlight()
    {
        HighlightSprite.renderer.enabled = true;
    }

    public void UnHighlight()
    {
        HighlightSprite.renderer.enabled = false;
    }

    public bool IsPressed
    {
        get { return !UnPressedSprite.renderer.enabled; }
    }

    public bool IsHighlighted
    {
        get { return HighlightSprite.renderer.enabled; }
    }
}
