﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour 
{
    public int CurrentLevel;
    public int EnergySpent;
    public int HighestLevelUnlocked = 1;

    public int CannonRank = 1;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        CheckSave();
    }

	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update() 
    {
	    
	}

    void CheckSave()
    {
        if (PlayerPrefs.HasKey("Level"))
        {
            Debug.Log("Key set.");
            HighestLevelUnlocked = PlayerPrefs.GetInt("Level");
        }
        else
        {
            Debug.Log("Key not set.");
            HighestLevelUnlocked = 1;
            PlayerPrefs.SetInt("Level", 1);
        }
    }
}
