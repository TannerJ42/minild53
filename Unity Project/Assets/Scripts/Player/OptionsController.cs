﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OptionsController : MonoBehaviour
{
    public GameObject BackButton;

    ButtonController backButton;

    int currentButtonIndex = 0;
    ButtonController currentButton;

    List<ButtonController> buttons;

    float timeSinceButtonChange = 0;
    const float timeBetweenButtonChanges = .3f;

    // Use this for initialization
    void Start()
    {
        buttons = new List<ButtonController>();

        backButton = BackButton.GetComponent<ButtonController>();

        buttons.Add(backButton);

        currentButton = backButton;
        currentButton.Highlight();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInputs();

        timeSinceButtonChange += Time.deltaTime;
    }

    void FixedUpdate()
    {
        if (!currentButton.IsHighlighted)
            currentButton.Highlight();
    }

    void ChangeScene()
    {
        if (currentButton == backButton)
            Application.LoadLevel("Main Menu");
    }

    void CheckInputs()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            currentButton.Press();
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (currentButton.IsPressed)
            {
                currentButton.UnPress();
                ChangeScene();
            }
        }

        if (!currentButton.IsPressed &&
            timeSinceButtonChange > timeBetweenButtonChanges)
        {
            if (Input.GetAxis("Vertical") > 0)
            {
                ChangeButton(-1);
            }
            if (Input.GetAxis("Vertical") < 0)
            {
                ChangeButton(1);
            }
        }
    }

    void ChangeButton(int direction)
    {
        currentButton.UnHighlight();

        currentButtonIndex += direction;

        if (currentButtonIndex < 0)
            currentButtonIndex = buttons.Count - 1;
        if (currentButtonIndex > buttons.Count - 1)
            currentButtonIndex = 0;


        currentButton = buttons[currentButtonIndex];
        currentButton.Highlight();

        timeSinceButtonChange = 0;
    }
}
