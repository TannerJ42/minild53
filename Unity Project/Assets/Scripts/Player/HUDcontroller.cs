﻿using UnityEngine;
using System.Collections;

public class HUDcontroller : MonoBehaviour 
{
    public GUIText HPText;
    public GUIText EnergyText;
    public GUIText EnergySpentText;

    public ShipController player;

    int currentHP = -1;
    int currentEnergy = -1;

	// Use this for initialization
	void Start() 
    {
        GameObject GameManager = GameObject.FindGameObjectWithTag("GameManager");
        GameState gameState = GameManager.GetComponent<GameState>();

        EnergySpentText.text = "Energy Spent: " + gameState.EnergySpent;
	}
	
	// Update is called once per frame
	void Update() 
    {
        if (currentHP != player.HP)
            RefreshHP();
        if (currentEnergy != player.Energy)
            RefreshEnergy();
	}

    private void RefreshHP()
    {
        currentHP = player.HP;
        HPText.text = "Current HP: " + currentHP;
    }

    private void RefreshEnergy()
    {
        currentEnergy = player.Energy;
        EnergyText.text = "Current Energy: " + currentEnergy;
    }
}
