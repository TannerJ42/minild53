﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LSController : MonoBehaviour
{
    public GameObject Level1;
    public GameObject Level2;
    public GameObject Back;

    ButtonController level1;
    ButtonController level2;
    ButtonController back;

    int currentButtonIndex = 0;
    ButtonController currentButton;

    List<ButtonController> buttons;

    float timeSinceButtonChange = 0;
    const float timeBetweenButtonChanges = .3f;

    GameState gameState;

    // Use this for initialization
    void Start()
    {
        buttons = new List<ButtonController>();

        level1 = Level1.GetComponent<ButtonController>();
        level2 = Level2.GetComponent<ButtonController>();
        back = Back.GetComponent<ButtonController>();

        buttons.Add(level1);
        buttons.Add(level2);
        buttons.Add(back);

        currentButton = level1;
        currentButton.Highlight();

        GameObject GameManager = GameObject.FindGameObjectWithTag("GameManager");
        gameState = GameManager.GetComponent<GameState>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInputs();

        timeSinceButtonChange += Time.deltaTime;
    }

    void FixedUpdate()
    {
        if (!currentButton.IsHighlighted)
            currentButton.Highlight();
    }

    void ChangeScene()
    {
        if (currentButton == level1)
            StartGame(1);
        if (currentButton == level2)
            StartGame(2);
        if (currentButton == back)
            Application.LoadLevel("Main Menu");
    }

    void StartGame(int level)
    {
        gameState.CurrentLevel = level;
        Application.LoadLevel("Gameplay");
    }

    void CheckInputs()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            currentButton.Press();
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (currentButton.IsPressed)
            {
                currentButton.UnPress();
                ChangeScene();
            }
        }

        if (!currentButton.IsPressed &&
            timeSinceButtonChange > timeBetweenButtonChanges)
        {
            if (Input.GetAxis("Vertical") > 0)
            {
                ChangeButton(-1);
            }
            if (Input.GetAxis("Vertical") < 0)
            {
                ChangeButton(1);
            }
        }
    }

    void ChangeButton(int direction)
    {
        currentButton.UnHighlight();

        currentButtonIndex += direction;

        if (currentButtonIndex < 0)
            currentButtonIndex = buttons.Count - 1;
        if (currentButtonIndex > buttons.Count - 1)
            currentButtonIndex = 0;


        currentButton = buttons[currentButtonIndex];
        currentButton.Highlight();

        timeSinceButtonChange = 0;
    }
}
