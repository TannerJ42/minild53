﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyProjectiles : MonoBehaviour 
{
	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update()
    {
	    
	}

    void OnCollisionExit2D(Collision2D c)
    {
        if (c.gameObject.tag == "Projectile")
            Destroy(c.gameObject);
    }
}
