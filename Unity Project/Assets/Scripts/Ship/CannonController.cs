﻿using UnityEngine;
using System.Collections;

public class CannonController : MonoBehaviour 
{
    public float FireSpeed;
    public GameObject Projectile;

    float timeSinceFired;

	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update() 
    {
        if (Input.GetAxis("Fire1") == 1 &&
            timeSinceFired >= FireSpeed)
        {
            Fire();
        }
	}

    void FixedUpdate()
    {
        timeSinceFired += Time.deltaTime;
    }

    void Fire()
    {
        timeSinceFired = 0;
        Instantiate(Projectile, new Vector3(transform.position.x, transform.position.y, transform.position.z + 1), transform.rotation);
    }
}
