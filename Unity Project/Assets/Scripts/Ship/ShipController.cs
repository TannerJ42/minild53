﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour 
{
    public float Speed;
    public int HP = 100;

    public int Energy = 0;

    Vector3 direction;

	// Use this for initialization
	void Start() 
    {
	    
	}
	
	// Update is called once per frame
	void Update() 
    {
        direction = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
	}

    void FixedUpdate()
    {
        transform.position = transform.position += direction * Speed * Time.deltaTime;
    }

    void TakeDamage(int damage)
    {
        HP -= damage;
        if (HP <= 0)
            Destroy(gameObject);
    }

    void AddEnergy(int energy)
    {
        Energy += energy;
    }
}
